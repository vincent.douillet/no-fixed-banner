function unfix() {
    document.querySelectorAll("div,header,nav").forEach(div => {
        var style = window.getComputedStyle(div);
        if(isFixedPosition(style) && isFullWidth(style) && isTopBottomPosition(style)) {
            div.style.position = "absolute";
        }
    });
}

function isFullWidth(elementStyle) {
    var pageWidth = document.body.clientWidth || document.documentElement.clientWidth || window.innerWidth;
    var elementWidth = elementStyle.getPropertyValue("width");
    return elementWidth === "100%" || elementWidth === pageWidth + "px";
}

function isFixedPosition(elementStyle) {
    return elementStyle.getPropertyValue("position") === "fixed";
}

function isTopBottomPosition(elementStyle) {
    return elementStyle.getPropertyValue("top") === "0px" || elementStyle.getPropertyValue("bottom") === "0px";
}